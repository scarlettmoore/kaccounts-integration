# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kaccounts-integration package.
# Wantoyèk <wantoyek@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kaccounts-integration\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-30 00:57+0000\n"
"PO-Revision-Date: 2023-01-01 21:27+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: src/kcm/package/contents/ui/AccountDetails.qml:20
#, kde-format
msgid "Account Details"
msgstr "Detail Akun"

#: src/kcm/package/contents/ui/AccountDetails.qml:59
#: src/kcm/package/contents/ui/main.qml:47
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: src/kcm/package/contents/ui/AccountDetails.qml:63
#: src/kcm/package/contents/ui/main.qml:51
#, kde-format
msgid "%1 account"
msgstr "%1 akun"

#: src/kcm/package/contents/ui/AccountDetails.qml:73
#, kde-format
msgctxt ""
"Button which spawns a dialog allowing the user to change the displayed "
"account's human-readable name"
msgid "Change Account Display Name"
msgstr "Ubah Nama Tampilan Akun"

#: src/kcm/package/contents/ui/AccountDetails.qml:81
#, kde-format
msgctxt "Heading for a list of services available with this account"
msgid "Use This Account For"
msgstr "Gunakan Akun Ini Untuk"

#: src/kcm/package/contents/ui/AccountDetails.qml:88
#, kde-format
msgid "Remove This Account"
msgstr "Hapus Akun Ini"

#: src/kcm/package/contents/ui/AccountDetails.qml:110
#, kde-format
msgctxt "A text shown when an account has no configurable services"
msgid ""
"There are no configurable services available for this account. You can still "
"change its display name by clicking the edit icon above."
msgstr ""
"Tidak ada layanan yang dapat diatur yang tersedia untuk akun ini. Kamu masih "
"bisa mengubah nama yang ditampilkan dengan mengeklik ikon edit di atas."

#: src/kcm/package/contents/ui/AvailableAccounts.qml:19
#, kde-format
msgid "Add New Account"
msgstr "Tambahkan Akun Baru"

#: src/kcm/package/contents/ui/main.qml:58
#, kde-format
msgctxt ""
"Tooltip for an action which will offer the user to remove the mentioned "
"account"
msgid "Remove %1"
msgstr "Hapus %1"

#: src/kcm/package/contents/ui/main.qml:75
#, kde-format
msgctxt "A text shown when a user has not yet added any accounts"
msgid "No accounts added yet"
msgstr "Belum ada akun yang ditambahkan"

#: src/kcm/package/contents/ui/main.qml:76
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add New Account...</interface> button below to add one"
msgstr ""
"Klik tombol <interface>Tambahkan Akun Baru...</interface> di bawah untuk "
"menambahkannya"

#: src/kcm/package/contents/ui/main.qml:88
#, kde-format
msgid "Add New Account..."
msgstr "Tambahkan Akun Baru..."

#: src/kcm/package/contents/ui/RemoveAccountDialog.qml:20
#, kde-format
msgctxt "The title for a dialog which lets you remove an account"
msgid "Remove Account?"
msgstr "Hapus Akun?"

#: src/kcm/package/contents/ui/RemoveAccountDialog.qml:23
#, kde-format
msgctxt ""
"The text for a dialog which lets you remove an account when both provider "
"name and account name are available"
msgid "Are you sure you wish to remove the \"%1\" account \"%2\"?"
msgstr "Apakah kamu yakin ingin menghapus akun \"%1\" \"%2\"?"

#: src/kcm/package/contents/ui/RemoveAccountDialog.qml:25
#, kde-format
msgctxt ""
"The text for a dialog which lets you remove an account when only the account "
"name is available"
msgid "Are you sure you wish to remove the account \"%1\"?"
msgstr "Apakah kamu yakin ingin menghapus akun \"%1\"?"

#: src/kcm/package/contents/ui/RemoveAccountDialog.qml:27
#, kde-format
msgctxt ""
"The text for a dialog which lets you remove an account when only the "
"provider name is available"
msgid "Are you sure you wish to remove this \"%1\" account?"
msgstr "Apakah kamu yakin ingin menghapus akun \"%1\" ini?"

#: src/kcm/package/contents/ui/RemoveAccountDialog.qml:32
#, kde-format
msgctxt ""
"The label for a button which will cause the removal of a specified account"
msgid "Remove Account"
msgstr "Hapus Akun"

#: src/kcm/package/contents/ui/RenameAccountDialog.qml:17
#, kde-format
msgctxt ""
"The title for a dialog which lets you set the human-readable name of an "
"account"
msgid "Rename Account"
msgstr "Ubah Nama Akun"

#: src/kcm/package/contents/ui/RenameAccountDialog.qml:31
#, kde-format
msgctxt ""
"Label for the text field used to enter a new human-readable name for an "
"account"
msgid "Enter new name:"
msgstr "Masukkan nama baru:"

#: src/kcm/package/contents/ui/RenameAccountDialog.qml:37
#, kde-format
msgctxt ""
"Text of a button which will cause the human-readable name of an account to "
"be set to a text specified by the user"
msgid "Set Account Name"
msgstr "Setel Nama Akun"

#: src/lib/changeaccountdisplaynamejob.cpp:71
#, kde-format
msgid "No account found with the ID %1"
msgstr "Tidak ada akun yang ditemukan dengan ID %1"

#: src/lib/changeaccountdisplaynamejob.cpp:76
#, kde-format
msgid "No accounts manager, this is not awesome."
msgstr "Tidak ada pengelola akun, ini tidak mengagumkan."

#: src/lib/changeaccountdisplaynamejob.cpp:81
#, kde-format
msgid "The display name cannot be empty"
msgstr "Nama yang ditampilkan tidak boleh kosong"

#: src/lib/createaccountjob.cpp:89
#, kde-format
msgctxt "The %1 is for plugin name, eg. Could not load UI plugin"
msgid "Could not load %1 plugin, please check your installation"
msgstr "Tidak bisa memuat plugin %1, silakan periksa instalanmu"

#: src/lib/createaccountjob.cpp:184
#, kde-format
msgid "Cancelled by user"
msgstr "Dibatalkan oleh pengguna"

#: src/lib/createaccountjob.cpp:261
#, kde-format
msgid "There was an error while trying to process the request: %1"
msgstr "Ada sebuah galat selagi mencoba memproses permintaan: %1"

#: src/lib/getcredentialsjob.cpp:53
#, kde-format
msgid "Could not find account"
msgstr "Tidak bisa menemukan akun"

#: src/lib/getcredentialsjob.cpp:68
#, kde-format
msgid "Could not find credentials"
msgstr "Tidak bisa menemukan kredensial"

#: src/lib/getcredentialsjob.cpp:78
#, kde-format
msgid "Could not create auth session"
msgstr "Tidak bisa membuat sesi autentikasi"
